import { Component, ViewChild } from '@angular/core';
import { Nav } from 'ionic-angular';
import { Subscription } from 'rxjs';

import { HomePage } from '../pages/home/home';
import { AuthProvider } from '../providers/auth/auth';

@Component({
  templateUrl: 'app.html',
})
export class MyApp {
  @ViewChild(Nav) public nav: Nav | undefined;
  public rootPage: any = HomePage;
  public pages: Array<{ title: string, component: any }>;
  public appVersion = '0.1.1'; // TODO: automate me or something
  public userEmail: string | null = null;
  private emailChangeSubscription: Subscription;

  constructor(private authProvider: AuthProvider) {
    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'Rules', component: 'RulesPage' }, // TODO: 'RulesPage' should be a global, fix everywhere
    ];
    this.emailChangeSubscription = this.authProvider.onEmailChange()
      .subscribe((email) => this.handleEmailChange(email));
  }

  public ionViewWillLeave(): void {
    this.emailChangeSubscription.unsubscribe();
  }

  public openPage(page: any): void {
    if (this.nav) {
      this.nav.setRoot(page.component);
    }
  }

  public openLogin(): void {
    if (this.nav) {
      // TODO: make global enum or something
      this.nav.setRoot('LoginPage');
    }
  }

  public openUser(): void {
    if (this.nav) {
      // TODO: make global enum or something
      this.nav.setRoot('UserPage');
    }
  }

  private handleEmailChange(email: string | null): void {
    this.userEmail = email;
  }
}
