import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { BehaviorSubject } from 'rxjs';
import { auth } from 'firebase';

@Injectable()
export class AuthProvider {
  private userEmailBehaviorSubject = new BehaviorSubject<string | null>(null);

  // TODO: add a system to redirect users after a login

  constructor(private afAuth: AngularFireAuth) {
    this.afAuth.auth.onAuthStateChanged((user) => {
      if (user) {
        this.userEmailBehaviorSubject.next(user.email);
      } else {
        this.userEmailBehaviorSubject.next(null);
      }
    });
  }

  public isLoggedIn(): Promise<boolean> {
    return new Promise((resolve) => {
      this.afAuth.authState.subscribe((user) => {
        if (user) {
          return resolve(true);
        }
        return resolve(false);
      });
    });
  }

  public onEmailChange(): BehaviorSubject<string | null> {
    return this.userEmailBehaviorSubject;
  }

  public login(email: string, password: string): Promise<auth.UserCredential> {
    return new Promise((resolve, reject) => {
      this.afAuth.auth.signInWithEmailAndPassword(email, password)
        .then((userCredential) => resolve(userCredential))
        .catch(error => reject(error));
    });
  }

  public logout(): void {
    this.afAuth.auth.signOut()
      .catch(err => console.error(err));
  }

  public sendPasswordResetEmail(email: string): Promise<void> {
    return new Promise((resolve, reject) => {
      this.afAuth.auth.sendPasswordResetEmail(email)
        .then(() => resolve())
        .catch(error => reject(error));
    });
  }
}
