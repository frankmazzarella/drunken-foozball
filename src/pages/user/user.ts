import { Component } from '@angular/core';
import { IonicPage, NavController, ToastController, AlertController } from 'ionic-angular';

import { HomePage } from '../home/home';
import { AuthProvider } from '../../providers/auth/auth';

@IonicPage({
  segment: 'user',
})
@Component({
  selector: 'page-user',
  templateUrl: 'user.html',
})
export class UserPage {
  constructor(
    private navCtrl: NavController,
    private authProvider: AuthProvider,
    private toastCtrl: ToastController,
    private alertCtrl: AlertController
  ) { }

  public ionViewCanEnter(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.authProvider.isLoggedIn()
        .then(isLoggedIn => {
          resolve(isLoggedIn);
          if (!isLoggedIn) {
            setTimeout(() => {
              this.navCtrl.setRoot('LoginPage'); // TODO: enum
            }, 0);
          }
        })
        .catch(error => reject(error));
    });
  }

  public changePassword(): void {
    this.presentAlert('too lazy to finish', 'now accepting pull requests for this functionality');
  }

  public changeEmail(): void {
    this.presentAlert('too lazy to finish', 'now accepting pull requests for this functionality');
  }

  public logout(): void {
    this.authProvider.logout();
    this.presentToast('You have been logged out');
    this.navCtrl.setRoot(HomePage);
  }

  private presentToast(message: string): void {
    this.toastCtrl.create({ message, duration: 3000 }).present();
  }

  private presentAlert(title: string, message: string): void {
    const alert = this.alertCtrl.create({
      title,
      message,
      buttons: [{ text: 'Got it' }],
    });
    alert.present();
  }
}
