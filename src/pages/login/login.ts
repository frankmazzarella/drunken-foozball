import { Component } from '@angular/core';
import { IonicPage, ToastController, NavController, AlertController } from 'ionic-angular';

import { AuthProvider } from '../../providers/auth/auth';
import { HomePage } from '../home/home';

@IonicPage({
  segment: 'login',
})
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  public email = '';
  public password = '';

  constructor(
    private authProvider: AuthProvider,
    private toastCtrl: ToastController,
    private navCtrl: NavController,
    private alertCtrl: AlertController
  ) { }

  public ionViewCanEnter(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.authProvider.isLoggedIn()
        .then(isLoggedIn => {
          resolve(!isLoggedIn);
          if (isLoggedIn) {
            setTimeout(() => {
              this.navCtrl.setRoot('UserPage'); // TODO: enum
            }, 0);
          }
        })
        .catch(error => reject(error));
    });
  }

  public login(): void {
    this.authProvider.login(this.email, this.password)
      .then((userCredential) => {
        if (userCredential.user) {
          this.presentToast(`You are now logged in as ${userCredential.user.email}`);
        }
        this.navCtrl.setRoot(HomePage);
      })
      .catch(err => this.presentToast(err));
  }

  public forgotPassword(): void {
    this.promptPasswordReset()
      .then((email) => {
        if (email === null) return;
        this.authProvider.sendPasswordResetEmail(email)
          .then(() => this.presentPasswordResetSent())
          .catch(error => this.presentToast(error));
      })
      .catch(error => this.presentToast(error));
  }

  private presentToast(message: string): void {
    this.toastCtrl.create({ message, duration: 3000 }).present();
  }

  private promptPasswordReset(): Promise<string | null> {
    return new Promise((resolve, reject) => {
      const prompt = this.alertCtrl.create({
        title: 'Reset Password',
        message: 'Enter your account email',
        inputs: [{ name: 'email', placeholder: 'Email' }],
        buttons: [
          { text: 'Cancel', handler: () => resolve(null) },
          { text: 'Reset Password', handler: (data) => resolve(data.email) },
        ],
      });
      prompt.present();
    });
  }

  private presentPasswordResetSent(): void {
    const prompt = this.alertCtrl.create({
      title: 'Go check your email',
      message: 'Not to worry, this happens to the best of us. Go grab a drink while a rescue email is on its way',
    });
    prompt.present();
  }
}
