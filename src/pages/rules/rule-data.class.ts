/* tslint:disable:max-line-length */

export default class RuleData {
  private static gameRules = [
    {
      chapterTitle: 'Introduction',
      sections: [
        {
          sectionTitle: 'Introduction',
          description: 'All Players and organizers must follow this rulebook.',
          subSections: [
            'The rulebook is the final source for any decisions, overruling all designated Officials. Talk to it. Explain your issue to it. If the rulebook does not respond immediately, you are not drunk enough, so drink.',
            'The rules of of the game of drunken foozball are conceived as much to facilitate drunkenness as to actually play a game.',
            'The objective is to make this shit legit.',
            'The aim of the rules is also to bring the game to a level of mutual agreement amongst the Players while seeming to accomplish nothing.',
            'We remind the reader that while the rulebook is written by drunk people and can be changed on a whim, um, uh...I don’t know where I’m going with this so just screw you.',
          ],
        },
      ],
    },
    {
      chapterTitle: 'Materials and Hardware',
      sections: [
        {
          sectionTitle: 'Table',
          subSections: [
            'The Official tables are whatever is in an official DFL sanctioned house. Current sanctioned DFL houses are as follows: 1. The Dougher Residence of Eynon. 2. The Harrington Residence of Throop.',
            'Sanctioned houses must provide level equipment. Level can be checked by any official leveling device, or smartphone, or like if you put the ball in the middle of the table, does it roll? Like just a little? You sure?',
          ],
        },
        {
          sectionTitle: 'Balls',
          subSections: [
            'The Official balls are chosen at the beginning of each match and voted on by all participating players.',
          ],
        },
        {
          sectionTitle: 'Handles',
          subSections: [
            'Handles must be securely attached to the bars. If a handle slips off, the organizer of the sanctioned house must drink.',
          ],
        },
        {
          sectionTitle: 'Other Accessories',
          subSections: [
            'All other accessories are authorized unless they are perceived to endanger the Player or other participants.',
            'Under no circumstances may the surface or sides of the table be modified by the accessories.',
          ],
        },
      ],
    },
    {
      chapterTitle: 'Rules',
      sections: [
        {
          sectionTitle: 'Code of Ethics',
          description: 'Any action of an unsportsmanlike or unethical nature (i.e. whining like a little bitch) during tournament play, in the tournament room, or on the grounds of the host facility, will be considered a violation of the Code of Ethics. Mutual respect between all players, officials and/or spectators is a requirement. It shall be the aim of every player and official to represent drunken foozball in the most positive and sportsmanlike manner possible. The standards of sportsmanship are reasonably expected to drop as participants become more intoxicated.',
          subSections: [
            'The penalty for breaking the Code of Ethics is to drink.',
          ],
        },
        {
          sectionTitle: 'The Match',
          description: 'Unless otherwise stated by the tournament directors, a match shall consist of 2 out of 3 games, unless you lose 2, then it’s 3 out of 5, but if you lose 3 it’s 5 out of 7. Each game shall consist of 10 points (for the winners at least).',
          subSections: [
            'The number of games played in the responsibility of the Tournament Directors and will be published in the tournament announcement.',
            'The penalty for breaking the official number of games or points played may be forfeiture of a match or expulsion from the tournament, and of course, drinking.',
          ],
        },
        {
          sectionTitle: 'To Start a Match',
          description: 'A round of taunting shall precede the start of the match. The team determined to be a bigger ‘bitch’ has the choice of table side or first serve. The more manly team has the other option.',
          subSections: [
            'Once a team has chosen either the table side or the first serve, they may not change their decision.',
            'The match officially starts once the ball has been put into play.',
          ],
        },
        {
          sectionTitle: 'Socials',
          description: 'At any time during a dead ball situation, any member of the tournament may call a social by yelling ‘social’.',
          subSections: [
            'All participants in the tournament must drink a reasonable amount of an alcoholic beverage.',
            'If a participant is underage, they may skip socials (wink wink). No really, kids. Skip the socials.',
            'A player who fails to participate in a social will have to drink more.',
            'SOCIALLLLLL!!!!!',
          ],
        },
        {
          sectionTitle: 'Serve Protocol',
          description: 'A serve is defined as putting the ball into play at the five-man rod at the beginning of a game, after a point is score or if the ball is awarded to a player following a rules infraction.',
          subSections: [
            'The serve shall begin with the ball in the hands of the serving player. The player must then follow the ready protocol.',
            'A player may spin the ball while serving, but if a goal is scored without the ball touching a player’s stick, then you’re a little bitch and it doesn’t count.',
            'Following the first serve of a match, subsequent serves shall be made by the team last scored upon (i.e Frank and Tom).',
            'If Tom cheats he drinks.',
          ],
        },
        {
          sectionTitle: 'Ready Protocol',
          description: 'The ready protocol will be used whenever putting the ball into play.',
          subSections: [
            'Before putting the ball into play the player in possession of the ball shall ask the opponent if he is ready.',
            'All players must be given a reasonable amount of time to finish a social and get back into position. Failure to allow a player to finish a social results in a penalty for the serving player. Drink.',
          ],
        },
        {
          sectionTitle: 'Ball in Play',
          subSections: [
            'Once a ball is put into play it shall remain in play until the ball is hit off the table, a dead ball is declared, or a point is scored.',
          ],
        },
        {
          sectionTitle: 'Ball Off the Table',
          description: 'If the ball leaves the playing area and strikes the scoring markers, light fixture, alcoholic drink, or any object that is not part of the table, the ball shall be declared off the table. If the ball hits the top of the side rails or cabinet ends and then immediately returns to the playing surface it will be considered in play.',
          subSections: [
            'The play area shall be defined as the area above the playing surface to the height of the sideboards of the cabinet. The top of the side rails and cabinet ends are only in play if the ball immediately returns to the playing surface.',
            'A ball entering the serving hole and then returning to the playfield is still considered \'in play.\'.',
            'If the power of a player’s shot or pass causes the ball to go off the table it will be put back into play by the opponent.',
            'A player may not perform any shot that causes the ball to lob or volley over the opponent\'s rods.',
          ],
        },
        {
          sectionTitle: 'Dead Ball',
          description: 'A ball shall be declared a dead ball when it has completely stopped its motion and is not within reach of any player figure.',
          subSections: [
            'The ball shall be put back into play through the serving hole by whoever grabs it first.',
            'A player may not blow on a ball or move the table in any fashion in an attempt to move the ball within reach of a player figure.',
          ],
        },
        {
          sectionTitle: 'Time Out',
          description: 'Each team is allowed two time outs per game during which the players may leave the table. Such time outs shall not exceed 1 minute.',
          subSections: [
            'Either team may take the full minute, even if the team that called the time out does not wish to take the full allotment.',
            'In any doubles event, either team may switch positions during a time out.',
            'A player who removes both hands from the handles and turns completely away from the table while the ball is in play shall be deemed to have requested a time out.',
          ],
        },
        {
          sectionTitle: 'Beer Runs',
          description: 'Any player may declare they are out of alcohol to request a beer run. This does not count as a time out.',
          subSections: [
            'A member not involved in the current match may be asked to retrieve an alcoholic beverage. If they refuse, they’re a little bitch.',
            'A player may be excused from only 1 social if they’re alcoholic beverage is empty.',
          ],
        },
        {
          sectionTitle: 'Points Scored',
          description: 'A ball entering the goal shall count as a point, as long as it was legally scored. A ball which enter the goal but returns to the playing surface and/or leaves the table does not count as a goal.',
          subSections: [
            'If a point is not counted on the scoring markers and both teams agree that it was previously scored and inadvertently not marked up, the point shall count.',
            'When a point is scored, one marker is moved (ONE, UNCLE TOMMY! JUST ONE!).',
          ],
        },
        {
          sectionTitle: 'Table Sides',
          subSections: [
            'At the end of each game, teams may switch sides of the table before play of the next game can begin.',
          ],
        },
        {
          sectionTitle: 'Change of Positions',
          description: 'In any doubles event, players may only play the two rods normally designated for his position. Once the ball is put into play, the players must play the same position until a point is scored.',
          subSections: [
            'Either team may switch positions during a time out, between points, or between games.',
            'Once a team has switched positions, they may not switch back until after the ball has been put back into play.',
            'Illegally switching positions results in a drink.',
          ],
        },
        {
          sectionTitle: 'Spinning the Rods',
          description: 'Spinning of the rods is illegal. Bitch.',
          subSections: [
            'Spinning is defined as the rotation of any soccer figure more than 360 degrees before or more than 360 degrees after striking the ball. In calculating the 360 degrees you do not add the degrees spun prior to striking the ball to the degrees spun after striking the ball. Drink.',
            'Exceptions will be made for rookie players that do not appreciate the finer things in life.',
          ],
        },
        {
          sectionTitle: 'Jarring',
          description: 'Jarring, sliding, or lifting of the table while the ball is in play is illegal. Whether or not the table jarring is done intentionally is of no consequence.',
          subSections: [
            'Drink.',
          ],
        },
        {
          sectionTitle: 'Reaching into the Playing Area',
          description: 'It is illegal for a player to reach into the play area while the ball is in play.',
          subSections: [
            'A spinning ball is still considered live, even if it is not in reach of a player figure.',
          ],
        },
        {
          sectionTitle: 'Alterations to the Table',
          description: 'Playing area - no changes can be made that would affect the interior playing characteristics of the table by any player.',
          subSections: [
            'A player cannot wipe sweat, spit, or any foreign substance on his hand before wiping ball marks off the table.',
            'A player may not place anything on the rods or handles.',
          ],
        },
        {
          sectionTitle: 'Language and Behavior',
          subSections: [
            'Ummm...yep.',
          ],
        },
        {
          sectionTitle: 'Rules, Decisions, and Appeals',
          subSections: [
            'You\'re wrong. Drink.',
          ],
        },
      ],
    },
    {
      chapterTitle: 'The Official Committee',
      sections: [
        {
          sectionTitle: 'Committee',
          description: 'The Official Drunken foozball Committee currently consists of the founding members:',
          subSections: [
            'Thomas \'Rev\' Dougher',
            'Samuel \'Bear\' Fumanti',
            'Frank \'Woodsy\' Mazzarella',
            'Samuel \'Lep\' Fumenti III',
          ],
        },
      ],
    },
  ];

  public static getRules(): any { // TODO: any
    return RuleData.gameRules;
  }
}
