import { Component, ViewChild } from '@angular/core';
import { IonicPage, Content, NavParams, NavController } from 'ionic-angular';

import GameRules from './rule-data.class';

@IonicPage({
  segment: 'rules/:section',
})
@Component({
  selector: 'page-rules',
  templateUrl: 'rules.html',
})
export class RulesPage {
  public currentYear = new Date().getFullYear();
  public gameRules = GameRules.getRules();
  @ViewChild(Content) content: Content | undefined;

  constructor(
    private navParams: NavParams,
    private navCtrl: NavController
  ) { }

  public ionViewDidEnter(): void {
    const activeSection = this.navParams.get('section');
    if (activeSection) {
      this.scrollToSection(activeSection);
    }
  }

  public setActiveSection(elementId: string): void {
    this.navCtrl.setRoot('RulesPage', { section: elementId });
  }

  public scrollToTop(): void {
    this.scrollToPosition(0, 0);
  }

  private scrollToSection(section: string): void {
    const element: HTMLElement | null = document.getElementById(section);
    if (element) {
      const scrollToX = 0;
      const scrollToY = element.offsetTop - 10;
      this.scrollToPosition(scrollToX, scrollToY);
    }
  }

  private scrollToPosition(scrollToX: number, scrollToY: number): void {
    if (this.content) {
      this.content.scrollTo(scrollToX, scrollToY, 1500);
    }
  }
}
